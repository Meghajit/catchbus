CREATE SEQUENCE ticket_id_seq;
ALTER TABLE ticket
  ALTER COLUMN id SET DEFAULT nextval('ticket_id_seq');
ALTER SEQUENCE ticket_id_seq
  OWNED BY ticket.id;