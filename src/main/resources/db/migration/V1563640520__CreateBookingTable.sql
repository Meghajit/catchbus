CREATE TABLE booking
(
  id          INTEGER PRIMARY KEY,
  person_id   INTEGER REFERENCES person (id) NOT NULL,
  ticket_id   INTEGER REFERENCES ticket (id) NOT NULL,
  booked_date DATE                           NOT NULL
)