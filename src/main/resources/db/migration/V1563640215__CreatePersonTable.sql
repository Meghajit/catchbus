CREATE TABLE person
(
  id           INTEGER PRIMARY KEY,
  fname        TEXT NOT NULL,
  lname        TEXT NOT NULL,
  phone_number TEXT NOT NULL
)