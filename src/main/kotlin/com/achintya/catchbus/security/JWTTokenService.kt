package com.achintya.catchbus.security

import arrow.core.Either
import io.jsonwebtoken.Claims
import io.jsonwebtoken.impl.DefaultJwtParser
import org.springframework.stereotype.Service

@Service
class JWTTokenService {
    fun parseToken(token: String): Either<InvalidRole, UserRole> {
        return try {
            val unsignedToken = token.split(".")
            val tokenPayload = ".${unsignedToken[1]}."
            val parser = DefaultJwtParser()
            val parse = parser.parse(tokenPayload)
            val userRole = UserRole(
                role = ValidRole.valueOf(
                    ((parse.body as Claims)["role"] as String)
                        .capitalize()
                )
            )
            Either.right(userRole)
        } catch (e: Exception) {
            Either.left(InvalidRole)
        }
    }
}

object InvalidRole

enum class ValidRole {
    USER, ADMIN
}

data class UserRole(
    val role: ValidRole
)