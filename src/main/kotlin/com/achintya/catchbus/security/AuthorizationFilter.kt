package com.achintya.catchbus.security

import arrow.core.Either
import org.apache.logging.log4j.util.Strings.isBlank
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
@Order(Int.MAX_VALUE)
class AuthorizationFilter(val jwtTokenService: JWTTokenService) : javax.servlet.Filter {
    data class RouteTable(
        val url: Regex,
        val method: String
    )

    companion object {
        val ADMIN_ROUTES = listOf(
            RouteTable(Regex("^/api/v1/tickets$"), "PUT")
        )
    }

    override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain) {
        val httpResponse = response as HttpServletResponse
        val httpRequest = request as HttpServletRequest
        val token = httpRequest.getHeader("Authorization")
        if (isBlank(token)) {
            httpResponse.status = HttpStatus.UNAUTHORIZED.value()
        } else {
            val user = jwtTokenService.parseToken(token)
            when (user) {
                is Either.Left -> {
                    httpResponse.status = HttpStatus.UNAUTHORIZED.value()
                }
                is Either.Right -> {
                    val access = checkPrivilege(request, user.b.role)
                    when (access) {
                        true -> chain.doFilter(request, response)
                        false -> httpResponse.status = HttpStatus.UNAUTHORIZED.value()
                    }
                }
            }
        }
    }

    private fun checkPrivilege(request: HttpServletRequest, role: ValidRole): Boolean {
        val privilegedRoute = ADMIN_ROUTES.any { route ->
            route.url.matches(request.requestURI) && route.method == request.method
        }
        return (privilegedRoute && role == ValidRole.ADMIN) || !privilegedRoute
    }
}