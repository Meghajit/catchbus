package com.achintya.catchbus.bus

import com.achintya.catchbus.ticket.TicketRepository
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service

@Service
class InitializerService(private val ticketRepository: TicketRepository) {

    @EventListener(ApplicationReadyEvent::class)
    fun resetTickets() {
        ticketRepository.initializeTickets()
    }
}