package com.achintya.catchbus.bus

import com.achintya.catchbus.booking.BookingRepository
import com.achintya.catchbus.person.Person
import com.achintya.catchbus.person.PersonRepository
import com.achintya.catchbus.ticket.Ticket
import com.achintya.catchbus.ticket.TicketRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.Objects.isNull

@RestController
@Transactional
class BusController(
    private val ticketRepository: TicketRepository,
    private val bookingRepository: BookingRepository,
    val personRepository: PersonRepository
) {

    @GetMapping("/api/v1/tickets/{ticketId}")
    fun getTicketStatus(@PathVariable ticketId: Int): ResponseEntity<TicketResponse> {
        return try {
            val ticket = ticketRepository.findTicket(ticketId)!!
            ResponseEntity.ok(TicketResponse(ticket))
        } catch (ex: NullPointerException) {
            ResponseEntity.notFound().build()
        }
    }

    @GetMapping("/api/v1/tickets")
    fun getTicketsWithStatus(@RequestParam open: Boolean): ResponseEntity<Set<TicketResponse>> {
        val ticketSet = ticketRepository.findTicketsWithStatus(open)
        return ResponseEntity.ok(ticketSet.map { it -> TicketResponse(it) }.toSet())
    }

    @GetMapping("/api/v1/tickets/{ticketId}/persons")
    fun getPersonDetailsOfTicket(@PathVariable ticketId: Int): ResponseEntity<Person> {
        return try {
            val booking = bookingRepository.find(ticketId)!!
            val person = personRepository.find(booking.personId)!!
            ResponseEntity.ok(person)
        } catch (ex: NullPointerException) {
            ResponseEntity.notFound().build()
        }
    }

    @PutMapping("/api/v1/tickets/{ticketId}")
    fun resetTicketBooking(
        @PathVariable ticketId: Int, @RequestParam open: Boolean
    ): ResponseEntity<TicketResponse> {
        if (open) {
            val ticketStatus = ticketRepository.findTicket(ticketId)?.open
            when (ticketStatus) {
                true -> return ResponseEntity.ok(
                    TicketResponse(
                        Ticket(
                            id = ticketId,
                            open = true
                        )
                    )
                )
                false -> {
                    ticketRepository.updateTicketStatus(ticketId, true)
                    bookingRepository.delete(ticketId)
                    return ResponseEntity.ok(
                        TicketResponse(
                            Ticket(
                                id = ticketId,
                                open = true
                            )
                        )
                    )
                }
                else -> return ResponseEntity.badRequest().build()
            }
        } else {
            return ResponseEntity.badRequest().build()
        }
    }

    @PostMapping("/api/v1/tickets/{ticketId}")
    fun postTicketBooking(
        @PathVariable ticketId: Int, @RequestParam open: Boolean, @RequestBody person: PersonDetails
    ): ResponseEntity<TicketCreationResponse> {
        if (!open) {
            val ticket = ticketRepository.findTicket(ticketId)
            if (isNull(ticket)) {
                return ResponseEntity.badRequest().build()
            } else {
                if (!ticket!!.open) {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).build()
                } else {
                    val bookingPerson = personRepository.findPerson(
                        person.fname, person.lname, person.phoneNumber
                    )
                    if (isNull(bookingPerson)) {
                        ticketRepository.updateTicketStatus(ticketId, false)
                        val personInsertedId = personRepository
                            .insert(person.fname, person.lname, person.phoneNumber)!!
                        val bookingId = bookingRepository.insert(ticketId, personInsertedId)!!
                        return ResponseEntity.ok(
                            TicketCreationResponse(
                                ticketId = ticketId,
                                ticketStatus = Status.CLOSED,
                                bookingId = bookingId,
                                personDetails = Person(personInsertedId, person.fname, person.lname, person.phoneNumber)
                            )
                        )
                    } else {
                        ticketRepository.updateTicketStatus(ticketId, false)
                        val bookingId = bookingRepository.insert(
                            ticketId,
                            bookingPerson!!.id
                        )!!
                        return ResponseEntity.ok(
                            TicketCreationResponse(
                                ticketId = ticketId,
                                ticketStatus = Status.CLOSED,
                                bookingId = bookingId,
                                personDetails = bookingPerson
                            )
                        )
                    }
                }
            }
        } else {
            return ResponseEntity.badRequest().build()
        }
    }

    @PutMapping("/api/v1/tickets")
    fun resetTickets(@RequestParam open: Boolean): ResponseEntity<HttpStatus> {
        return if (!open) {
            ResponseEntity.badRequest().build()
        } else {
            ticketRepository.updateAllTicketStatusAsOpen(   )
            bookingRepository.deleteAll()
            ResponseEntity.status(HttpStatus.OK).build()
        }
    }
}

class TicketResponse(ticket: Ticket) {
    val id = ticket.id
    val ticketStatus = ticket.open.run {
        when (this) {
            true -> Status.OPEN
            else -> Status.CLOSED
        }
    }
}

data class PersonDetails(
    val fname: String,
    val lname: String,
    val phoneNumber: String
)

data class TicketCreationResponse(
    val ticketId: Int,
    val ticketStatus: Status,
    val bookingId: Int,
    val personDetails: Person
)

enum class Status {
    OPEN, CLOSED
}