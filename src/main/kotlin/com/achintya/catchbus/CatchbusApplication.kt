package com.achintya.catchbus

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CatchbusApplication

fun main(args: Array<String>) {
    runApplication<CatchbusApplication>(*args)
}
