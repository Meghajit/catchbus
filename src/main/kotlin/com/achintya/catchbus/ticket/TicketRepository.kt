package com.achintya.catchbus.ticket

import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet

interface TicketRepository {
    fun findTicket(ticketId: Int): Ticket?
    fun findTicketsWithStatus(openStatus: Boolean): Set<Ticket>
    fun updateTicketStatus(ticketId: Int, openStatus: Boolean): Boolean
    fun updateAllTicketStatusAsOpen(): Boolean
    fun initializeTickets(): Boolean
}

@Repository
class TicketRepositoryImpl(private val jdbc: NamedParameterJdbcTemplate) : TicketRepository {
    override fun initializeTickets(): Boolean {
        val checkRowCountQuery = "SELECT COUNT(*) FROM ticket"
        try {
            val countTickets = jdbc.queryForObject(checkRowCountQuery, MapSqlParameterSource(), Int::class.java)!!
            if (countTickets != TOTAL_TICKETS_AVAILABLE) {
                val arrayOfValues = Array(TOTAL_TICKETS_AVAILABLE) { "(TRUE)" }.joinToString()
                val insertTicketsQuery = "INSERT INTO ticket (open) VALUES $arrayOfValues"

                val ticketsInserted = jdbc.update(insertTicketsQuery, MapSqlParameterSource())
                if (ticketsInserted == TOTAL_TICKETS_AVAILABLE) {
                    return true
                }
            }
        } catch (ex: Exception) {
            return false
        }
        return false
    }

    override fun updateAllTicketStatusAsOpen(): Boolean {
        val query = "UPDATE ticket SET open = TRUE"
        val rowsUpdated = jdbc.update(query, MapSqlParameterSource())
        return when (rowsUpdated) {
            TOTAL_TICKETS_AVAILABLE -> true
            else -> false
        }
    }

    override fun updateTicketStatus(ticketId: Int, openStatus: Boolean): Boolean {
        val query = "UPDATE ticket SET open = :openStatus WHERE id = :ticketId"
        val params = MapSqlParameterSource()
            .addValue("openStatus", openStatus)
            .addValue("ticketId", ticketId)
        val rowsUpdated = jdbc.update(query, params)
        return when (rowsUpdated) {
            1 -> true
            else -> false
        }
    }

    override fun findTicketsWithStatus(openStatus: Boolean): Set<Ticket> {
        val query = "SELECT * FROM ticket WHERE open = :openStatus"
        val params = MapSqlParameterSource()
            .addValue("openStatus", openStatus)
        return jdbc.query(query, params, TicketMapper()).toSet()
    }

    override fun findTicket(ticketId: Int): Ticket? {
        val query = "SELECT * FROM ticket WHERE id = :id"
        val params = MapSqlParameterSource()
            .addValue("id", ticketId)
        return try {
            jdbc.queryForObject(query, params, TicketMapper())
        } catch (ex: EmptyResultDataAccessException) {
            null
        }
    }
}

class TicketMapper : RowMapper<Ticket> {
    override fun mapRow(rs: ResultSet, rowNum: Int): Ticket? {
        return Ticket(
            id = rs.getInt("id"),
            open = rs.getBoolean("open")
        )
    }
}