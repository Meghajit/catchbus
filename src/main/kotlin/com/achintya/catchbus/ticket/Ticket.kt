package com.achintya.catchbus.ticket

const val TOTAL_TICKETS_AVAILABLE = 40

data class Ticket(
    val id: Int,
    val open: Boolean
)