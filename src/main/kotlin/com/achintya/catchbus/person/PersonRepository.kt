package com.achintya.catchbus.person

import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet

interface PersonRepository {
    fun insert(fname: String, lname: String, phoneNumber: String): Int?
    fun find(personId: Int): Person?
    fun findPerson(fname:String, lname:String, phoneNumber: String): Person?
}

@Repository
class PersonRepositoryImpl(private val jdbc: NamedParameterJdbcTemplate) : PersonRepository {
    override fun find(personId: Int): Person? {
        val query = "SELECT * from person where id = :personId"
        val params = MapSqlParameterSource()
            .addValue("personId", personId)
        return try {
            jdbc.queryForObject(query, params, PersonMapper())
        } catch (ex: EmptyResultDataAccessException) {
            null
        }
    }

    override fun findPerson(fname:String, lname:String, phoneNumber: String): Person? {
    val query = "SELECT * from person where fname = :fname AND lname=:lname AND phone_number = :phoneNumber"
        val params = MapSqlParameterSource()
            .addValue("fname", fname)
            .addValue("lname", lname)
            .addValue("phoneNumber", phoneNumber)
        return try {
            jdbc.queryForObject(query, params, PersonMapper())
        } catch (ex: EmptyResultDataAccessException) {
            null
        }
    }

    override fun insert(fname: String, lname: String, phoneNumber: String): Int? {
        val query = "INSERT INTO person(fname, lname, phone_number) VALUES(:fname, :lname, :phoneNumber) " +
            "RETURNING id"
        val params = MapSqlParameterSource()
            .addValue("fname", fname)
            .addValue("lname", lname)
            .addValue("phoneNumber", phoneNumber)
        return try {
            jdbc.queryForObject(query, params, Int::class.java)
        } catch (ex: EmptyResultDataAccessException) {
            null
        }
    }
}

class PersonMapper : RowMapper<Person> {
    override fun mapRow(rs: ResultSet, rowNum: Int): Person? {
        return Person(
            id = rs.getInt("id"),
            fname = rs.getString("fname"),
            lname = rs.getString("lname"),
            phoneNumber = rs.getString("phone_number")
        )
    }
}