package com.achintya.catchbus.person

data class Person(
    val id: Int,
    val fname: String,
    val lname: String,
    val phoneNumber: String
)