package com.achintya.catchbus.booking

import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

interface BookingRepository {
    fun insert(ticketId: Int, personId: Int): Int?
    fun find(ticketId: Int): Booking?
    fun delete(ticketId: Int): Boolean
    fun deleteAll(): Boolean
}

@Repository
class BookingRepositoryImpl(private val jdbc: NamedParameterJdbcTemplate) : BookingRepository {
    override fun deleteAll(): Boolean {
        val query = "DELETE FROM booking"
        val deletedBookings = jdbc.update(query, MapSqlParameterSource())
        return deletedBookings > 0
    }

    override fun delete(ticketId: Int): Boolean {
        val query = "DELETE FROM booking WHERE ticket_id = :ticketId"
        val params = MapSqlParameterSource()
            .addValue("ticketId", ticketId)
        val deletedBookings = jdbc.update(query, params)
        return when (deletedBookings) {
            1 -> true
            else -> false
        }
    }

    override fun find(ticketId: Int): Booking? {
        val query = "SELECT * from booking where ticket_id = :ticketId"
        val params = MapSqlParameterSource()
            .addValue("ticketId", ticketId)

        val resultSet = jdbc.query(query, params, BookingMapper())
        return when (resultSet.size) {
            1 -> return resultSet[0]
            else -> null
        }
    }

    override fun insert(ticketId: Int, personId: Int): Int? {
        val query = "INSERT INTO booking(person_id, ticket_id, booked_date) " +
            "VALUES(:personId, :ticketId, to_date(:bookedDate, 'YYYY-MM-DD')) RETURNING id"
        val currentDate = ZonedDateTime.now(ZoneId.of("Asia/Kolkata"))
        val formattedDate = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(currentDate)
        val params = MapSqlParameterSource()
            .addValue("ticketId", ticketId)
            .addValue("personId", personId)
            .addValue("bookedDate", formattedDate)

        return try {
            jdbc.queryForObject(query, params, Int::class.java)
        } catch (ex: EmptyResultDataAccessException) {
            null
        }
    }
}

class BookingMapper : RowMapper<Booking> {
    override fun mapRow(rs: ResultSet, rowNum: Int): Booking? {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        return Booking(
            id = rs.getInt("id"),
            ticketId = rs.getInt("ticket_id"),
            personId = rs.getInt("person_id"),
            bookedDate = LocalDate.parse(
                rs.getDate("booked_date").toString(),
                formatter
            )
        )
    }
}