package com.achintya.catchbus.booking

import java.time.LocalDate

data class Booking(
    val id: Int,
    val personId: Int,
    val ticketId: Int,
    val bookedDate: LocalDate
)