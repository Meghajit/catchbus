package com.achintya.catchbus.security

import arrow.core.Either
import io.jsonwebtoken.Jwts;
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.junit.Test

class JWTTokenServiceTest {
    private val jwtTokenService: JWTTokenService = JWTTokenService()

    @Test
    @Throws(Exception::class)
    fun parseToken_givenACorruptedJWTToken_throwsException() {
        jwtTokenService.parseToken("Hello Corrupted Token!")
    }

    @Test
    fun parseToken_givenAValidTokenWithAnInvalidRole_returnsInvalidRoleObject() {
        val tokenRole = "{\"role\":\"CORRUPTED ROLE\"}"
        val jwtToken: String = Jwts.builder().setHeader(
            mapOf(
                Pair("alg", "HS256"),
                Pair("typ", "JWT")
            )
        ).setPayload(tokenRole).compact()

        val role = jwtTokenService.parseToken(jwtToken)

        when (role) {
            is Either.Left -> assertThat(role.a).isEqualTo(InvalidRole)
            is Either.Right -> fail("Expected role to equal InvalidRole. Found ${role.b}")

        }
    }

    @Test
    fun parseToken_givenAValidTokenWithAValidRole_returnsUserRoleObject() {
        val tokenRole = "{\"role\":\"ADMIN\"}"
        val jwtToken: String = Jwts.builder().setHeader(
            mapOf(
                Pair("alg", "HS256"),
                Pair("typ", "JWT")
            )
        ).setPayload(tokenRole).compact()

        val role = jwtTokenService.parseToken(jwtToken)

        when (role) {
            is Either.Left -> fail("Expected role to equal UserRole(role = ValidRole.ADMIN). Found ${role.a}")
            is Either.Right -> assertThat(role.b).isEqualTo(UserRole(ValidRole.ADMIN))

        }
    }
}