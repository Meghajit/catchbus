package com.achintya.catchbus.security

import arrow.core.Either
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import javax.servlet.FilterChain

class AuthorizationFilterTest {

    private val jwtTokenService: JWTTokenService = mock()
    private val filter: AuthorizationFilter = AuthorizationFilter(jwtTokenService)

    @Test
    fun doFilter_whenNoTokenProvided_returns401AndDoesNotCallFilterChain() {
        val request = MockHttpServletRequest("GET", "/api/v1/tickets")
        val chain: FilterChain = mock()
        val response = MockHttpServletResponse()
        filter.doFilter(request, response, chain)
        assertThat(response.status).isEqualTo(401)
        verifyZeroInteractions(chain)
    }

    @Test
    fun doFilter_whenInvalidTokenProvided_returns401AndDoesNotCallFilterChain() {
        whenever(jwtTokenService.parseToken("Bearer the-token")).thenReturn(Either.left(InvalidRole))
        val request = MockHttpServletRequest("GET", "/api/v1/greeting")
        request.addHeader("Authorization", "Bearer the-token")
        val chain: FilterChain = mock()
        val response = MockHttpServletResponse()

        filter.doFilter(request, response, chain)

        assertThat(response.status).isEqualTo(401)
        verifyZeroInteractions(chain)
    }

    @Test
    fun doFilter_whenAdminTokenProvidedAndAdminRoutesAccessed_callsFilterChain() {
        whenever(jwtTokenService.parseToken(".etyty.")).thenReturn(Either.right(UserRole(ValidRole.ADMIN)))
        val request = MockHttpServletRequest("PUT", "/api/v1/tickets")
        request.addHeader("Authorization", ".etyty.")
        val chain: FilterChain = mock()
        val response = MockHttpServletResponse()

        filter.doFilter(request, response, chain)

        verify(chain).doFilter(request, response)
        assertThat(response.status).isEqualTo(200)
    }

    @Test
    fun doFilter_whenUserTokenProvidedAndUnsecureRoutesAccessed_callsFilterChain() {
        whenever(jwtTokenService.parseToken(".etyty.")).thenReturn(Either.right(UserRole(ValidRole.USER)))
        val request = MockHttpServletRequest("GET", "/api/v1/tickets")
        request.addHeader("Authorization", ".etyty.")
        val chain: FilterChain = mock()
        val response = MockHttpServletResponse()

        filter.doFilter(request, response, chain)

        verify(chain).doFilter(request, response)
        assertThat(response.status).isEqualTo(200)
    }

    @Test
    fun doFilter_whenUserTokenProvidedAndAdminRoutesAccessed_returns401AndDoesNotCallFilterChain() {
        whenever(jwtTokenService.parseToken(".etyty.")).thenReturn(Either.right(UserRole(ValidRole.USER)))
        val request = MockHttpServletRequest("PUT", "/api/v1/tickets")
        request.addHeader("Authorization", ".etyty.")
        val chain: FilterChain = mock()
        val response = MockHttpServletResponse()

        filter.doFilter(request, response, chain)

        assertThat(response.status).isEqualTo(401)
        verifyZeroInteractions(chain)
    }
}