package com.achintya.catchbus.person

import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate

class PersonRepositoryTest {

    private val jdbc: NamedParameterJdbcTemplate = mock()
    private val personRepository = PersonRepositoryImpl(jdbc)

    @Test
    fun insert_whenPassedPersonDetails_insertsAPersonAndReturnsTheId() {
        val fname = "Sunayana"
        val lname = "Mazumdar"
        val phoneNumber = "987654321"
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        whenever(
            jdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                eq(Int::class.java)
            )
        )
            .thenReturn(14)

        val personId = personRepository.insert(fname, lname, phoneNumber)

        verify(jdbc, times(1)).queryForObject(
            eq("INSERT INTO person(fname, lname, phone_number) VALUES(:fname, :lname, :phoneNumber) RETURNING id"),
            argument.capture(),
            eq(Int::class.java)
        )
        assertThat(argument.value.values).isEqualTo(
            mapOf(
                Pair("fname", fname),
                Pair("lname", lname),
                Pair("phoneNumber", phoneNumber)
            )
        )
        assertThat(personId).isEqualTo(14)
    }

    @Test
    fun insert_whenPassedPersonDetails_returnsNullWhenInsertionFails() {
        val fname = "Sunayana"
        val lname = "Mazumdar"
        val phoneNumber = "987654321"
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        whenever(
            jdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                eq(Int::class.java)
            )
        )
            .thenThrow(EmptyResultDataAccessException::class.java)

        val personId = personRepository.insert(fname, lname, phoneNumber)

        verify(jdbc, times(1)).queryForObject(
            eq("INSERT INTO person(fname, lname, phone_number) VALUES(:fname, :lname, :phoneNumber) RETURNING id"),
            argument.capture(),
            eq(Int::class.java)
        )
        assertThat(argument.value.values).isEqualTo(
            mapOf(
                Pair("fname", fname),
                Pair("lname", lname),
                Pair("phoneNumber", phoneNumber)
            )
        )
        assertThat(personId).isNull()
    }

    @Test
    fun find_whenPassedAPersonId_returnsTheCorrespondingPerson() {
        val personId = 194
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        val mapperArgumentCaptor = ArgumentCaptor
            .forClass(PersonMapper::class.java)
        whenever(
            jdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                Mockito.any<PersonMapper>()
            )
        ).thenReturn(
            Person(
                id = personId,
                fname = "Sunayana",
                lname = "Mazumdar",
                phoneNumber = "83883"
            )
        )

        val person = personRepository.find(personId)

        verify(jdbc, times(1)).queryForObject(
            eq("SELECT * from person where id = :personId"),
            argument.capture(),
            mapperArgumentCaptor.capture()
        )
        assertThat(argument.value.values).isEqualTo(
            mapOf(
                Pair("personId", personId)
            )
        )
        assertThat(mapperArgumentCaptor.value).isInstanceOf(PersonMapper::class.java)
        assertThat(person).isEqualToComparingFieldByFieldRecursively(
            Person(
                id = personId,
                fname = "Sunayana",
                lname = "Mazumdar",
                phoneNumber = "83883"
            )
        )
    }

    @Test
    fun find_whenPassedAPersonId_returnsNullWhenNoCorrespondingPersonFound() {
        val personId = 0
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        val mapperArgumentCaptor = ArgumentCaptor
            .forClass(PersonMapper::class.java)
        whenever(
            jdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                Mockito.any<PersonMapper>()
            )
        )
            .thenThrow(EmptyResultDataAccessException::class.java)

        val person = personRepository.find(personId)

        verify(jdbc, times(1)).queryForObject(
            eq("SELECT * from person where id = :personId"),
            argument.capture(),
            mapperArgumentCaptor.capture()
        )
        assertThat(argument.value.values).isEqualTo(
            mapOf(
                Pair("personId", personId)
            )
        )
        assertThat(mapperArgumentCaptor.value).isInstanceOf(PersonMapper::class.java)
        assertThat(person).isNull()
    }
}