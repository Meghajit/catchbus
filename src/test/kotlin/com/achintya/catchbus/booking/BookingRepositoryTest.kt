package com.achintya.catchbus.booking

import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class BookingRepositoryTest {
    private val jdbc: NamedParameterJdbcTemplate = mock()
    private val bookingRepository = BookingRepositoryImpl(jdbc)

    @Test
    fun insert_whenPassedANewTicketIdAndNewPersonId_insertsBookingAndReturnsBookingId() {
        val ticketId = 1
        val personId = 13
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        val currentFormattedDate = DateTimeFormatter
            .ofPattern("yyyy-MM-dd")
            .format(
                ZonedDateTime.now(ZoneId.of("Asia/Kolkata"))
            )
        whenever(
            jdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                eq(Int::class.java)
            )
        )
            .thenReturn(99)

        val bookingId = bookingRepository.insert(ticketId, personId)

        verify(jdbc, times(1)).queryForObject(
            eq(
                "INSERT INTO booking(person_id, ticket_id, booked_date) " +
                    "VALUES(:personId, :ticketId, to_date(:bookedDate, 'YYYY-MM-DD')) RETURNING id"
            ),
            argument.capture(),
            eq(Int::class.java)
        )
        assertThat(argument.value.values).isEqualTo(
            mapOf(
                Pair("personId", personId),
                Pair("ticketId", ticketId),
                Pair("bookedDate", currentFormattedDate)
            )
        )
        assertThat(bookingId).isEqualTo(99)
    }

    @Test
    fun insert_whenPassedATicketIdAndPersonId_returnsNullIfInsertionFails() {
        val ticketId = 1
        val personId = 13
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        val currentFormattedDate = DateTimeFormatter
            .ofPattern("yyyy-MM-dd")
            .format(
                ZonedDateTime.now(ZoneId.of("Asia/Kolkata"))
            )
        whenever(
            jdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                eq(Int::class.java)
            )
        )
            .thenThrow(EmptyResultDataAccessException::class.java)

        val bookingId = bookingRepository.insert(ticketId, personId)

        verify(jdbc, times(1)).queryForObject(
            eq(
                "INSERT INTO booking(person_id, ticket_id, booked_date) " +
                    "VALUES(:personId, :ticketId, to_date(:bookedDate, 'YYYY-MM-DD')) RETURNING id"
            ),
            argument.capture(),
            eq(Int::class.java)
        )
        assertThat(argument.value.values).isEqualTo(
            mapOf(
                Pair("personId", personId),
                Pair("ticketId", ticketId),
                Pair("bookedDate", currentFormattedDate)
            )
        )
        assertThat(bookingId).isNull()
    }

    @Test
    fun find_whenPassedATicketId_returnsCorrespondingBooking() {
        val ticketId = 45
        val paramsArgumentCaptor = ArgumentCaptor
            .forClass(MapSqlParameterSource::class.java)
        val mapperArgumentCaptor = ArgumentCaptor
            .forClass(BookingMapper::class.java)
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val bookingDate = LocalDate.parse("2019-07-22", formatter)
        whenever(
            jdbc.query(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                Mockito.any<BookingMapper>()
            )
        ).thenReturn(
            listOf(
                Booking(
                    id = 76,
                    personId = 13,
                    ticketId = ticketId,
                    bookedDate = bookingDate
                )
            )
        )

        val booking = bookingRepository.find(ticketId)

        verify(jdbc, times(1)).query(
            eq("SELECT * from booking where ticket_id = :ticketId"),
            paramsArgumentCaptor.capture(),
            mapperArgumentCaptor.capture()
        )
        assertThat(paramsArgumentCaptor.value.values).isEqualTo(
            mapOf(
                Pair("ticketId", ticketId)
            )
        )
        assertThat(mapperArgumentCaptor.value).isInstanceOf(BookingMapper::class.java)
        assertThat(booking).isEqualToComparingFieldByFieldRecursively(
            Booking(
                id = 76,
                personId = 13,
                ticketId = ticketId,
                bookedDate = bookingDate
            )
        )
    }

    @Test
    fun find_whenPassedATicketId_returnsNullIfNoBookingFound() {
        val ticketId = 45
        val paramsArgumentCaptor = ArgumentCaptor
            .forClass(MapSqlParameterSource::class.java)
        val mapperArgumentCaptor = ArgumentCaptor
            .forClass(BookingMapper::class.java)
        whenever(
            jdbc.query(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                Mockito.any<BookingMapper>()
            )
        ).thenReturn(
            emptyList()
        )

        val booking = bookingRepository.find(ticketId)

        verify(jdbc, times(1)).query(
            eq("SELECT * from booking where ticket_id = :ticketId"),
            paramsArgumentCaptor.capture(),
            mapperArgumentCaptor.capture()
        )
        assertThat(paramsArgumentCaptor.value.values).isEqualTo(
            mapOf(
                Pair("ticketId", ticketId)
            )
        )
        assertThat(mapperArgumentCaptor.value).isInstanceOf(BookingMapper::class.java)
        assertThat(booking).isNull()
    }

    @Test
    fun delete_whenPassedATicketId_deletesTheCorrespondingBooking() {
        val ticketId = 999
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        whenever(
            jdbc.update(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>()
            )
        )
            .thenReturn(1)

        val success = bookingRepository.delete(ticketId)

        verify(jdbc, times(1)).update(
            eq("DELETE FROM booking WHERE ticket_id = :ticketId"),
            argument.capture()
        )
        assertThat(argument.value.values).isEqualTo(
            mapOf(
                Pair("ticketId", ticketId)
            )
        )
        assertThat(success).isTrue()
    }

    @Test
    fun delete_whenPassedATicketId_returnsNullWhenBookingNotFound() {
        val ticketId = 34
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        whenever(
            jdbc.update(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>()
            )
        )
            .thenReturn(0)

        val success = bookingRepository.delete(ticketId)

        verify(jdbc, times(1)).update(
            eq("DELETE FROM booking WHERE ticket_id = :ticketId"),
            argument.capture()
        )
        assertThat(argument.value.values).isEqualTo(
            mapOf(
                Pair("ticketId", ticketId)
            )
        )
        assertThat(success).isFalse()
    }

    @Test
    fun deleteAll_deletesAllBookingsAndReturnsTrue() {
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)

        whenever(
            jdbc.update(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>()
            )
        )
            .thenReturn(23)

        val success = bookingRepository.deleteAll()

        verify(jdbc, times(1)).update(
            eq("DELETE FROM booking"),
            argument.capture()
        )
        assertThat(argument.value.values).isEqualTo(
            emptyMap<String, String>()
        )
        assertThat(success).isTrue()
    }

    @Test
    fun deleteAll_returnsFalseWhenNoBookingsToDelete() {
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)

        whenever(
            jdbc.update(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>()
            )
        )
            .thenReturn(0)

        val success = bookingRepository.deleteAll()

        verify(jdbc, times(1)).update(
            eq("DELETE FROM booking"),
            argument.capture()
        )
        assertThat(argument.value.values).isEqualTo(
            emptyMap<String, String>()
        )
        assertThat(success).isFalse()
    }
}