package com.achintya.catchbus.ticket

import com.achintya.catchbus.booking.BookingMapper
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.omg.CORBA.Object
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate

class TicketRepositoryImplTest {

    private val jdbc: NamedParameterJdbcTemplate = mock()
    private val ticketRepository: TicketRepository = TicketRepositoryImpl(jdbc)

    @Test
    fun findTicket_whenPassedATicketId_returnsTicketDetails() {
        val ticketId = 123
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        val mapperArgumentCaptor = ArgumentCaptor
            .forClass(BookingMapper::class.java)
        whenever(
            jdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                Mockito.any<TicketMapper>()
            )
        )
            .thenReturn(
                Ticket(
                    id = 123,
                    open = false
                )
            )

        val ticket = ticketRepository.findTicket(ticketId)

        assertThat(ticket).isEqualToComparingFieldByField(
            Ticket(
                id = 123,
                open = false
            )
        )
        verify(jdbc, times(1)).queryForObject(
            eq("SELECT * FROM ticket WHERE id = :id"),
            argument.capture(),
            mapperArgumentCaptor.capture()
        )
        assertThat(argument.value.values["id"]).isEqualTo(ticketId)
        assertThat(mapperArgumentCaptor.value).isInstanceOf(TicketMapper::class.java)
    }

    @Test
    fun findTicket_whenPassedATicketIdWhichDoesNotExist_returnsNull() {
        val ticketId = 55
        whenever(
            jdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                Mockito.any<TicketMapper>()
            )
        )
            .thenThrow(EmptyResultDataAccessException::class.java)

        val ticket = ticketRepository.findTicket(ticketId)

        assertThat(ticket).isNull()
    }

    @Test
    fun findTicketsWithStatus_returnsSetOfAllTicketsHavingRequestedStatus() {
        val ticketStatusQueried = false
        val ticketList = listOf(
            Ticket(
                id = 123,
                open = ticketStatusQueried
            ),
            Ticket(
                id = 644,
                open = ticketStatusQueried
            )
        )
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        val mapperArgumentCaptor = ArgumentCaptor
            .forClass(BookingMapper::class.java)
        whenever(
            jdbc.query(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                Mockito.any<TicketMapper>()
            )
        )
            .thenReturn(ticketList)

        val ticket = ticketRepository.findTicketsWithStatus(ticketStatusQueried)

        assertThat(ticket).isEqualTo(
            setOf(
                Ticket(
                    id = 123,
                    open = ticketStatusQueried
                ),
                Ticket(
                    id = 644,
                    open = ticketStatusQueried
                )
            )
        )
        verify(jdbc, times(1)).query(
            eq("SELECT * FROM ticket WHERE open = :openStatus"),
            argument.capture(),
            mapperArgumentCaptor.capture()
        )
        assertThat(argument.value.values["openStatus"]).isEqualTo(ticketStatusQueried)
        assertThat(mapperArgumentCaptor.value).isInstanceOf(TicketMapper::class.java)
    }

    @Test
    fun updateTicketStatus_whenPassedATicketIdAndStatus_updatesTheStatusOfTheTicketAndReturnsTrue() {
        val ticketId = 123
        val openStatus = true
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        whenever(
            jdbc.update(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>()
            )
        )
            .thenReturn(1)

        val success = ticketRepository.updateTicketStatus(ticketId, openStatus)

        assertThat(success).isEqualTo(true)
        verify(jdbc, times(1)).update(
            eq("UPDATE ticket SET open = :openStatus WHERE id = :ticketId"),
            argument.capture()
        )
        assertThat(argument.value.values).isEqualTo(
            mapOf(
                Pair("ticketId", ticketId),
                Pair("openStatus", openStatus)
            )
        )
    }

    @Test
    fun updateTicketStatus_whenPassedATicketIdAndStatus_returnsFalseWhenTicketIdDoesNotExist() {
        val ticketId = 999
        val openStatus = false
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        whenever(
            jdbc.update(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>()
            )
        )
            .thenReturn(0)

        val success = ticketRepository.updateTicketStatus(ticketId, openStatus)

        assertThat(success).isEqualTo(false)
        verify(jdbc, times(1)).update(
            eq("UPDATE ticket SET open = :openStatus WHERE id = :ticketId"),
            argument.capture()
        )
        assertThat(argument.value.values).isEqualTo(
            mapOf(
                Pair("ticketId", ticketId),
                Pair("openStatus", openStatus)
            )
        )
    }

    @Test
    fun updateAllTicketStatusAsOpen_updatesStatusOfAllTicketsAsOpenAndReturnsTrue() {
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        whenever(
            jdbc.update(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>()
            )
        )
            .thenReturn(40)

        val success = ticketRepository.updateAllTicketStatusAsOpen()

        assertThat(success).isTrue()
        verify(jdbc, times(1)).update(
            eq("UPDATE ticket SET open = TRUE"),
            argument.capture()
        )
        assertThat(argument.value.values).isEqualTo(emptyMap<String, Object>())
    }

    @Test
    fun updateAllTicketStatusAsOpen_returnsFalseWhenUnableToUpdateStatusForSomeRows() {
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        whenever(
            jdbc.update(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>()
            )
        )
            .thenReturn(34)

        val success = ticketRepository.updateAllTicketStatusAsOpen()

        assertThat(success).isFalse()
        verify(jdbc, times(1)).update(
            eq("UPDATE ticket SET open = TRUE"),
            argument.capture()
        )
        assertThat(argument.value.values).isEqualTo(emptyMap<String, Object>())
    }

    @Test
    fun initializeTickets_whenTicketTableIsEmpty_insertsAllTicketsWithOpenStatusAndReturnsTrue() {
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        val arrayOfValues = Array(TOTAL_TICKETS_AVAILABLE) { "(TRUE)" }.joinToString()
        val orderVerifier = Mockito.inOrder(jdbc)

        whenever(
            jdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                Mockito.eq(Int::class.java)
            )
        )
            .thenReturn(0)
        whenever(
            jdbc.update(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>()
            )
        )
            .thenReturn(TOTAL_TICKETS_AVAILABLE)

        val success = ticketRepository.initializeTickets()

        assertThat(success).isTrue()
        orderVerifier.verify(jdbc, times(1)).queryForObject(
            eq("SELECT COUNT(*) FROM ticket"),
            argument.capture(),
            eq(Int::class.java)
        )
        assertThat(argument.value.values).isEqualTo(emptyMap<String, Object>())
        orderVerifier.verify(jdbc, times(1)).update(
            eq("INSERT INTO ticket (open) VALUES $arrayOfValues"),
            argument.capture()
        )
        assertThat(argument.value.values).isEqualTo(emptyMap<String, Object>())
    }

    @Test
    fun initializeTickets_whenTicketTableIsNotEmpty_doesNotInsertAnyTicketsAndReturnsFalse() {
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)

        whenever(
            jdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                Mockito.eq(Int::class.java)
            )
        )
            .thenReturn(40)

        val success = ticketRepository.initializeTickets()

        assertThat(success).isFalse()
        verify(jdbc, times(1)).queryForObject(
            eq("SELECT COUNT(*) FROM ticket"),
            argument.capture(),
            eq(Int::class.java)
        )
        verifyNoMoreInteractions(jdbc)
        assertThat(argument.value.values).isEqualTo(emptyMap<String, Object>())
    }

    @Test
    fun initializeTickets_whenTicketTableIsNotEmptyButInsertionFails_ReturnsFalse() {
        val argument = ArgumentCaptor.forClass(MapSqlParameterSource::class.java)
        val arrayOfValues = Array(TOTAL_TICKETS_AVAILABLE) { "(TRUE)" }.joinToString()
        val orderVerifier = Mockito.inOrder(jdbc)

        whenever(
            jdbc.queryForObject(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>(),
                Mockito.eq(Int::class.java)
            )
        )
            .thenReturn(0)
        whenever(
            jdbc.update(
                Mockito.anyString(),
                Mockito.any<MapSqlParameterSource>()
            )
        )
            .thenReturn(0)

        val success = ticketRepository.initializeTickets()

        assertThat(success).isFalse()
        orderVerifier.verify(jdbc, times(1)).queryForObject(
            eq("SELECT COUNT(*) FROM ticket"),
            argument.capture(),
            eq(Int::class.java)
        )
        assertThat(argument.value.values).isEqualTo(emptyMap<String, Object>())
        orderVerifier.verify(jdbc, times(1)).update(
            eq("INSERT INTO ticket (open) VALUES $arrayOfValues"),
            argument.capture()
        )
        assertThat(argument.value.values).isEqualTo(emptyMap<String, Object>())
    }
}