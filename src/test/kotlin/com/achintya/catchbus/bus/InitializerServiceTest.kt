package com.achintya.catchbus.bus

import com.achintya.catchbus.ticket.TicketRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Test

class InitializerServiceTest {
    private val ticketRepository: TicketRepository = mock()
    private val initializerService = InitializerService(ticketRepository)

    @Test
    fun initializeTickets_initializesTicketTable() {
        whenever(ticketRepository.initializeTickets()).thenReturn(true)

        initializerService.resetTickets()

        verify(ticketRepository, times(1)).initializeTickets()
    }
}