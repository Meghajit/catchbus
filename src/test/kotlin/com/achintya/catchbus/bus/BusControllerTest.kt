package com.achintya.catchbus.bus

import com.achintya.catchbus.booking.Booking
import com.achintya.catchbus.booking.BookingRepository
import com.achintya.catchbus.person.Person
import com.achintya.catchbus.person.PersonRepository
import com.achintya.catchbus.ticket.Ticket
import com.achintya.catchbus.ticket.TicketRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Before
import org.junit.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class BusControllerTest {
    private lateinit var mvc: MockMvc
    private val ticketRepository: TicketRepository = mock()
    private val bookingRepository: BookingRepository = mock()
    private val personRepository: PersonRepository = mock()

    @Before
    fun setup() {
        val controller = BusController(ticketRepository, bookingRepository, personRepository)
        mvc = MockMvcBuilders.standaloneSetup(controller).build()
    }

    @Test
    fun getTicketStatus_whenTicketExists_returnsTicketStatusWithStatus200() {
        whenever(ticketRepository.findTicket(12)).thenReturn(Ticket(12, true))

        mvc.perform(get("/api/v1/tickets/12").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("12"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.ticketStatus").value("OPEN"))

        verify(ticketRepository, times(1)).findTicket(12)
    }

    @Test
    fun getTicketStatus_whenTicketDoesNotExist_returnsStatus404() {
        whenever(ticketRepository.findTicket(12)).thenReturn(null)

        mvc.perform(get("/api/v1/tickets/12").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isNotFound)

        verify(ticketRepository, times(1)).findTicket(12)
    }

    @Test
    fun getTicketsWithStatus_whenTicketsExist_returnsTicketsWithRequestedStatusWithStatus200() {
        whenever(ticketRepository.findTicketsWithStatus(false)).thenReturn(
            setOf(
                Ticket(1, false),
                Ticket(23, false),
                Ticket(12, false)
            )
        )

        mvc.perform(get("/api/v1/tickets?open=false").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").value("1"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[0].ticketStatus").value("CLOSED"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[1].id").value("23"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[1].ticketStatus").value("CLOSED"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[2].id").value("12"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[2].ticketStatus").value("CLOSED"))

        verify(ticketRepository, times(1)).findTicketsWithStatus(false)
    }

    @Test
    fun getPersonDetailsOfTicket_whenTicketExists_returnsPersonDetailsWithStatus200() {
        val ticketId = 37
        val personId = 23
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val bookingDate = LocalDate.parse("2019-07-22", formatter)
        whenever(bookingRepository.find(ticketId)).thenReturn(
            Booking(
                id = 12,
                ticketId = ticketId,
                personId = personId,
                bookedDate = bookingDate
            )
        )
        whenever(personRepository.find(personId)).thenReturn(
            Person(
                id = personId,
                fname = "Sunayana",
                lname = "Mazumdar",
                phoneNumber = "74747"
            )
        )

        mvc.perform(get("/api/v1/tickets/37/persons").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("23"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.fname").value("Sunayana"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.lname").value("Mazumdar"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.phoneNumber").value("74747"))
    }

    @Test
    fun getPersonDetailsOfTicket_whenTicketDoesNotExistOrTicketIsOpen_returnsStatus404() {
        whenever(bookingRepository.find(37)).thenReturn(null)

        mvc.perform(get("/api/v1/tickets/37/persons").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isNotFound)
    }

    @Test
    fun resetTicketBooking_whenRequestParamOpenIsTrue_updatesTheTicketAsOpenAndReturnsStatus200() {
        whenever(ticketRepository.updateTicketStatus(13, true))
            .thenReturn(true)
        whenever(ticketRepository.findTicket(13)).thenReturn(Ticket(13, false))
        whenever(bookingRepository.delete(13))
            .thenReturn(true)

        mvc.perform(put("/api/v1/tickets/13?open=true").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("13"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.ticketStatus").value("OPEN"))

        verify(ticketRepository, times(1)).updateTicketStatus(13, true)
        verify(ticketRepository, times(1)).findTicket(13)
        verifyNoMoreInteractions(ticketRepository)
        verify(bookingRepository, times(1)).delete(13)
    }

    @Test
    fun resetTicketBooking_whenRequestParamOpenIsFalse_ReturnsStatus400() {
        mvc.perform(put("/api/v1/tickets/13?open=false").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isBadRequest)

        verifyZeroInteractions(ticketRepository)
        verifyZeroInteractions(bookingRepository)
    }

    @Test
    fun resetTicketBooking_whenTicketIdIsInvalid_ReturnsStatus400() {
        mvc.perform(put("/api/v1/tickets/999?open=true").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isBadRequest)

        verify(ticketRepository, times(1)).findTicket(999)
        verifyZeroInteractions(bookingRepository)
    }

    @Test
    fun resetTicketBooking_whenTicketIsAlreadyOpen_doesNotDeleteBookingAndReturnsStatus200() {
        whenever(ticketRepository.findTicket(12)).thenReturn(Ticket(12, true))

        mvc.perform(put("/api/v1/tickets/12?open=true").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("12"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.ticketStatus").value("OPEN"))

        verify(ticketRepository, times(1)).findTicket(12)
        verifyNoMoreInteractions(ticketRepository)
        verifyZeroInteractions(bookingRepository)
    }

    @Test
    fun postTicketBooking_whenRequestParamIsTrue_returnsStatus400() {
        mvc.perform(post("/api/v1/tickets/13?open=true").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isBadRequest)

        verifyZeroInteractions(ticketRepository)
        verifyZeroInteractions(bookingRepository)
    }

    @Test
    fun postTicketBooking_whenTicketIdIsInvalid_ReturnsStatus400() {
        val body =
            "{\"fname\": \"Sunayana\", \"lname\": \"Mazumdar\", \"phoneNumber\": \"77328\"}"

        mvc.perform(
            post("/api/v1/tickets/999?open=false")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(body)
        )
            .andExpect(MockMvcResultMatchers.status().isBadRequest)

        verify(ticketRepository, times(1)).findTicket(999)
        verifyZeroInteractions(bookingRepository)
    }

    @Test
    fun postTicketBooking_whenTicketStatusIsClosed_returnsStatus403() {
        val body = "{\"fname\": \"Sunayana\", \"lname\": \"Mazumdar\", \"phoneNumber\": \"77328\"}"
        whenever(ticketRepository.findTicket(13)).thenReturn(Ticket(13, false))

        mvc.perform(
            post("/api/v1/tickets/13?open=false")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(body)
        )
            .andExpect(MockMvcResultMatchers.status().isForbidden)

        verify(ticketRepository, times(1)).findTicket(13)
        verifyNoMoreInteractions(ticketRepository)
        verifyZeroInteractions(personRepository)
        verifyZeroInteractions(bookingRepository)
    }

    @Test
    fun postTicketBooking_whenTicketStatusIsOpenAndPersonIsNew_insertPersonAndReturnsNewBookingDetailsWithStatus200() {
        val body = "{\"fname\": \"Sunayana\", \"lname\": \"Mazumdar\", \"phoneNumber\": \"77328\"}"
        whenever(ticketRepository.findTicket(13)).thenReturn(Ticket(13, true))
        whenever(personRepository.findPerson("Sunayana", "Mazumdar", "77328"))
            .thenReturn(null)
        whenever(personRepository.insert("Sunayana", "Mazumdar", "77328"))
            .thenReturn(4)
        whenever(bookingRepository.insert(13, 4))
            .thenReturn(6)

        mvc.perform(
            post("/api/v1/tickets/13?open=false")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(body)
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("$.ticketId").value("13"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.ticketStatus").value("CLOSED"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.bookingId").value("6"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.personDetails.id").value("4"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.personDetails.fname").value("Sunayana"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.personDetails.lname").value("Mazumdar"))
            .andExpect(
                MockMvcResultMatchers.jsonPath("$.personDetails.phoneNumber")
                    .value("77328")
            )

        verify(ticketRepository, times(1)).findTicket(13)
        verify(ticketRepository, times(1)).updateTicketStatus(13, false)
        verifyNoMoreInteractions(ticketRepository)
        verify(personRepository, times(1))
            .findPerson("Sunayana", "Mazumdar", "77328")
        verify(personRepository, times(1))
            .insert("Sunayana", "Mazumdar", "77328")
        verifyNoMoreInteractions(personRepository)
        verify(bookingRepository, times(1)).insert(13, 4)
        verifyNoMoreInteractions(bookingRepository)
    }

    @Test
    fun postTicketBooking_whenTicketStatusIsOpenAndUserIsExisting_returnsNewBookingDetailsWithStatus200() {
        val body = "{\"fname\": \"Sunayana\", \"lname\": \"Mazumdar\", \"phoneNumber\": \"77328\"}"
        whenever(ticketRepository.findTicket(13)).thenReturn(Ticket(13, true))
        whenever(personRepository.findPerson("Sunayana", "Mazumdar", "77328"))
            .thenReturn(Person(4, "Sunayana", "Mazumdar", "77328"))
        whenever(bookingRepository.insert(13, 4))
            .thenReturn(6)

        mvc.perform(
            post("/api/v1/tickets/13?open=false")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(body)
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("$.ticketId").value("13"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.ticketStatus").value("CLOSED"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.bookingId").value("6"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.personDetails.id").value("4"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.personDetails.fname").value("Sunayana"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.personDetails.lname").value("Mazumdar"))
            .andExpect(
                MockMvcResultMatchers.jsonPath("$.personDetails.phoneNumber")
                    .value("77328")
            )

        verify(ticketRepository, times(1)).findTicket(13)
        verify(ticketRepository, times(1)).updateTicketStatus(13, false)
        verifyNoMoreInteractions(ticketRepository)
        verify(personRepository, times(1))
            .findPerson("Sunayana", "Mazumdar", "77328")
        verifyNoMoreInteractions(personRepository)
        verify(bookingRepository, times(1)).insert(13, 4)
        verifyNoMoreInteractions(bookingRepository)
    }

    @Test
    fun resetTickets_whenRequestParamOpenIsFalse_returns400() {
        mvc.perform(put("/api/v1/tickets?open=false").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isBadRequest)

        verifyZeroInteractions(ticketRepository)
        verifyZeroInteractions(bookingRepository)
        verifyZeroInteractions(personRepository)
    }

    @Test
    fun resetTickets_whenRequestParamOpenIsTrue_resetsAllTicketsAsOpenAndDeletesAllBookingsAndReturnsStatus200() {
        mvc.perform(put("/api/v1/tickets?open=true").accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.status().isOk)

        verify(ticketRepository, times(1)).updateAllTicketStatusAsOpen()
        verifyNoMoreInteractions(ticketRepository)
        verify(bookingRepository, times(1)).deleteAll()
        verifyNoMoreInteractions(bookingRepository)
        verifyZeroInteractions(personRepository)
    }
}