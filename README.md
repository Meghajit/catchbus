# CatchBus

REST API Services to manage bus tickets

The following is a list of the API Endpoints which are currently available:-

| API  | METHOD | Description | Accessible to Users | Accessible to Admin |
| ------ | ------ | ------ | ------ | ------ |
|/api/v1/tickets/{:**id**}| GET | View Status of ticket | Yes |  Yes |
|  /api/v1/tickets?**open**={true/false} | GET | View all open/closed tickets | Yes |  Yes |
|  /api/v1/tickets/{:**id**}/users | GET | View details of person owning the ticket | Yes |  Yes |
|  /api/v1/tickets/{:**id**}/?**open**=false | POST | Close an open ticket and create booking for user | Yes |  Yes |
|  /api/v1/tickets/{:**id**}/?**open**=true | PUT | Open a closed ticket and delete the booking | Yes |  Yes |
|  /api/v1/tickets?**open**=true | PUT | Reset all tickets to status open and deletes all bookings | No |  Yes |

There exist three entities:  Ticket, Person and Booking. The database schema is 
as follows: -

```
                            Table "public.ticket"
 Column |  Type   | Collation | Nullable |              Default
--------+---------+-----------+----------+------------------------------------
 id     | integer |           | not null | nextval('ticket_id_seq'::regclass)
 open   | boolean |           | not null |
Indexes:
    "ticket_pkey" PRIMARY KEY, btree (id)
Referenced by:
    TABLE "booking" CONSTRAINT "booking_ticket_id_fkey" FOREIGN KEY (ticket_id) REFERENCES ticket(id)
```

```
                               Table "public.person"
    Column    |  Type   | Collation | Nullable |              Default
--------------+---------+-----------+----------+------------------------------------
 id           | integer |           | not null | nextval('person_id_seq'::regclass)
 fname        | text    |           | not null |
 lname        | text    |           | not null |
 phone_number | text    |           | not null |
Indexes:
    "person_pkey" PRIMARY KEY, btree (id)
Referenced by:
    TABLE "booking" CONSTRAINT "booking_person_id_fkey" FOREIGN KEY (person_id) REFERENCES person(id)
```


```
                               Table "public.booking"
   Column    |  Type   | Collation | Nullable |               Default
-------------+---------+-----------+----------+-------------------------------------
 id          | integer |           | not null | nextval('booking_id_seq'::regclass)
 person_id   | integer |           | not null |
 ticket_id   | integer |           | not null |
 booked_date | date    |           | not null |
Indexes:
    "booking_pkey" PRIMARY KEY, btree (id)
Foreign-key constraints:
    "booking_person_id_fkey" FOREIGN KEY (person_id) REFERENCES person(id)
    "booking_ticket_id_fkey" FOREIGN KEY (ticket_id) REFERENCES ticket(id)
```

All APIs require a Bearer Token in the `Authorization` header. You can create a 
JWT token by visiting https://jwt.io/

For generating a token for normal users, payload should be:
```
{
  "role": "USER"
}
```

For generating a token for admin, payload should be:
```
{
  "role": "ADMIN"
}
```

Other information about the APIs and the request structure is included in the
Swagger Documentation and the Postman Collection inside `/src/main/resources`.